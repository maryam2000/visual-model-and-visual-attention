# Visual Model and Visual Attention

- This includes two of my Advance Topics and Computational Neuroscience Course (Graduate) simulations.

- **Simulation 8:** Studied the sparse representation of natural image statistics as the receptive fields of simple cells in the primary visual cortex (V1).

- **Simulation 9:** Learning to predict where humans look, derive saliency maps according image features.
